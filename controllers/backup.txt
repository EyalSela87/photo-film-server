const compressFile = (fileName, loop, res) =>
{
  // this func will loop till the image size is lower than 1mb
  return new Promise((resolve, reject) =>
  {
    if (loop === 5)
    {
      resolve();
      return;
    }
    const compressionValues = {
      // lower the image quality every loop
      jpg: [60, 70, 80, 90, 100],
      png: [[20, 15, 10, 5, 2], [50, 40, 30, 20, 10]],
    };

    console.log("compressionValues png", compressionValues.png);
    console.log("compressionValues jpg", compressionValues.jpg);


    const filePath = path.join(__dirname, "../", "images", fileName);
    const newPath = path.join(__dirname, "../", "images", "toCpmpress", fileName);


    fs.rename(filePath, newPath, (err) => 
    {
      if (err) reject(err);
      // let filePath = path.join(__dirname, "../", "images", fileName)
    });




    console.log("COMPRESSING!!!!", loop, `${compressionValues.png[0][loop]}-${compressionValues.png[1][loop]}`);
    compress_images(
      "images/toCpmpress/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}",
      "images/compressed/",
      { compress_force: false, statistic: true, autoupdate: true },
      false,
      { jpg: { engine: "mozjpeg", command: ["-quality", compressionValues.jpg[loop]] } },
      { png: { engine: "pngquant", command: [`--quality=${compressionValues.png[0][loop]}-${compressionValues.png[1][loop]}`, "-o"] } },
      // { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
      // { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
      { svg: { engine: "svgo", command: "--multipass" } },
      {
        gif: { engine: "gifsicle", command: ["--colors", "64", "--use-col=web"] },
      },
      function (err, completed)
      {
        if (completed === true)
        {
          console.log("complete")
          console.log("newPath", newPath)
          fs.unlink(newPath, (err) =>
          {
            if (err) reject(err);

            const compressedPath = path.join(__dirname, '../', "images", "compressed", fileName);
            console.log("compressedPath", compressedPath);

            if (loop > 4)
            {
              resolve();
              return;
            }

            console.log("loop check", loop);

            fs.stat(compressedPath, (err, fileStats) =>
            {
              if (err) reject(err);

              if (!fileStats) console.log("error", loop, compressedPath)

              fileSize = fileStats.size;

              console.log("fileSize", fileSize);


              if (fileSize > 1000000)
              {
                fs.rename(compressedPath, filePath, (err) => 
                {
                  if (err) reject(err);
                  compressFile(fileName, loop + 1)
                    .then(() => resolve())
                    .catch(err => reject(err));
                  // let filePath = path.join(__dirname, "../", "images", fileName)
                });
              }
              else resolve();
            })

          })
          // Doing something.

        }
        if (err) reject(err);
      }
    );

    // imagemin([['images/toCpmpress/*.{jpg,png}']], {
    //   destination: 'images/compressed/',
    //   plugins: [
    //     imageminJpegtran(),
    //     imageminPngquant({
    //       quality: [0.6, 0.8]
    //     })
    //   ]
    // })
    //   .then(() => resolve())
    //   .catch(err => reject(err))


    //   compress_images(
    //     `images/toCpmpress/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}`,
    //     `images/compressed/`,
    //     { compress_force: false, statistic: true, autoupdate: true },
    //     false,
    //     {
    //       jpg: {
    //         engine: "mozjpeg",
    //         command: ["-quality", 60],
    //       },
    //     },
    //     {
    //       png: {
    //         engine: "pngquant",
    //         command: [
    //           `--quality=${compressionValues.png[0]}-${compressionValues.png[1]}`,
    //           "-o",
    //         ],
    //       },
    //     },
    //     { svg: { engine: "svgo", command: "--multipass" } },
    //     {
    //       gif: {
    //         engine: "gifsicle",
    //         command: ["--colors", "64", "--use-col=web"],
    //       },
    //     },
    //     (err, completed) =>
    //     {
    //       if (completed === true)
    //       {
    //         console.log("complete");
    //         const oldPath = path.join(
    //           __dirname,
    //           "../",
    //           "images",
    //           "compressed",
    //           fileName
    //         );
    //         const newPath = path.join(__dirname, "../", "images", fileName);

    //         fs.rename(oldPath, newPath, (err) =>
    //         {
    //           if (err) reject(err);
    //           let filePath = path.join(__dirname, "../", "images", fileName);

    //           fs.stat(filePath, (err, fileStats) =>
    //           {
    //             if (err) reject(err);
    //             fileSize = fileStats.size;
    //             console.log("fileSize", fileSize);

    //             if (loop > 10)
    //             {
    //               resolve();
    //               return;
    //             }
    //             if (fileSize > 1000000)
    //               compressFile(fileName, loop + 1).then(() => resolve());
    //             else resolve();
    //           });
    //         });
    //       } else
    //       {
    //         console.log("erorrrorr");
    //       }
    //       if (err) reject(err);
    //     }
    //   );
  });
};













      // compressFile(req.file.filename, 0)
      //   .then(() => res.status(200).json(Response(true, data, null)))
      //   .catch((err) =>
      //   {
      //     err.statusCode = 422; next(err);
      //   });