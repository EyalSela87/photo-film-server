const bcrypt = require('bcryptjs');

const User = require('../models/user');
const Response = require('../util/response');


exports.createUser = (req, res, next) =>
{
    // receive user info from reqrest body
    const user = {
        name: req.body.name || null,
        email: req.body.email || null,
        password: req.body.password || null,
        verifyPassword: req.body.verifyPassword || null
    }
    // validate user info
    const errors = verifyUserInfo(user);
    // if there an errors return them to the client
    if (errors.length > 0)
    {
        const error = new Error(errors);
        error.statusCode = 422;
        throw error;
    }
    //find user by email
    User.findOne({ where: { email: user.email } })
        .then(userWithSameEmail => 
        {// find if email already in used
            if (userWithSameEmail !== null) throw new Error(user.email + " is already in used");
            // if email not in used hash the password
            return bcrypt.hash(user.password, 12);
        })
        .then(hashedPassword =>
        {// create user with register info and hashed password
            const data = { name: user.name, email: user.email, password: hashedPassword };
            return User.create(data);
        })
        .then(result =>
        {// return the result to the client
            res.status(200).json(Response(true, result, null));
        })
        .catch(err => { err.statusCode = 422; next(err) });
}

exports.loginUser = (req, res, next) =>
{
    // receive user info from reqrest body
    const email = req.body.email || null;
    const password = req.body.password || null;
    let user;

    //#region email validation
    if (!email || email === "")
    {
        const error = new Error("email field is require");
        error.statusCode = 422;
        throw error;
    }

    else if (!validateEmail(email))
    {
        const error = new Error("email is not valid");
        error.statusCode = 422;
        throw error;
    }
    //#endregion

    // find user by email
    User.findOne({ where: { email: email } })
        .then(userDb =>
        {
            // if no user has found trow error to client
            if (userDb === null)
                throw new Error(email + " is not exist");
            //if user found store the info
            user = userDb;
            //compare between the password and the hash password
            return bcrypt.compare(password, userDb.password);
        })
        .then(isMatch =>
        {
            // if compare faild throw error to client
            if (!isMatch)
                throw new Error("password is not match");
            // if compare succeed return the user info to the client
            res.status(200).json(Response(true, user, null));
        })
        .catch(err => { err.statusCode = 422; next(err) })
}

const verifyUserInfo = (info) =>
{
    const errors = [];

    if (!info.name || info.name === "") errors.push('name field is require');
    else
    {
        const noSpaceName = info.name.split(' ').join('');
        if (noSpaceName.length < 3) errors.push("name must cointain atleast 3 characters");
    }

    if (!info.email || info.email === "") errors.push('email field is require');
    else if (!validateEmail(info.email)) errors.push("email is not valid");

    if (!info.password || info.password === "") errors.push('password field is require');
    else
    {
        const noSpacePassword = info.password.split(' ').join('');
        if (noSpacePassword.length < 5) errors.push("password must cointain atleast 5 characters & no spaces");
    }

    console.log("info.verifyPassword", info.verifyPassword)

    if (info.password !== info.verifyPassword) errors.push("passwords are not match");

    return errors;
}

const validateEmail = (email) =>
{
    const check = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return check.test(String(email).toLowerCase());
}