const path = require("path");
const fs = require("fs");
const compress_images = require("compress-images");
const Response = require('../util/response');

exports.UploadFiles = (req, res, next) =>
{
    //#region OLD CODE
    // let userId = req.body.userId;
    // let files = req.files;

    // let vacationsImages = req.files.map(v => {
    //     let ob = {
    //         vacationId: result['dataValues']['id'],
    //         image: 'images/' + v.filename,
    //     }

    //     return ob;
    // })
    //#endregion
    // domain URL
    const domain = "http://localhost:3000";
    // const domain = "https://404040.co.il/firumu";

    // if not file has sent, throw an error
    if (!req.file)
    {
        const error = new Error("file not found");
        error.statusCode = 404;
        throw error;
    }
    //get file path
    let filePath = path.join(__dirname, "../", "uploads", "images", req.fileName);
    //get file stats
    fs.stat(filePath, (err, fileStats) =>
    {
        // set needed file stats in an object
        const data = { path: domain + "/images/" + req.fileName, name: req.fileName };
        // get file size and if file need compression as a boolean
        const fileSize = fileStats.size;
        const compressNeeded = fileSize > 1000000;
        // compress file if needed
        if (compressNeeded)
        {
            // compress file and send response to client
            compressFile2(data.name)
                .then(() => res.status(200).json(Response(true, data, null)))
                .catch(err => { err.statusCode = 422; next(err) });
        }
        //else response to client
        else res.status(200).json(Response(true, data, null));
    });
}
// func that compress file
const compressFile2 = (fileName) =>
{
    return new Promise((resolve, reject) =>
    {
        //get images folder path
        const destinationPath = path.join(__dirname, "../", "uploads", "images", fileName);
        //get toCompress path
        const toCompressPath = path.join(__dirname, "../", "uploads", "toCpmpress", fileName);
        //move the file from images folder to 'toCompressed' folder
        fs.rename(destinationPath, toCompressPath, (err) => { if (err) reject(err); });
        // compress image (will automatcly move the image for 'toCompressed' folder to 'compressed' folder)
        compress_images("uploads/toCpmpress/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}",
            "uploads/compressed/",
            { compress_force: false, statistic: true, autoupdate: true },
            false,
            { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
            { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
            { svg: { engine: "svgo", command: "--multipass" } },
            { gif: { engine: "gifsicle", command: ["--colors", "64", "--use-col=web"] } },
            function (error, completed, statistic)
            {
                console.log("-------------");
                console.log(error);
                console.log(completed);
                console.log(statistic);
                console.log("-------------");
                //after compression completed
                if (completed === true)
                {
                    //get compressed path  
                    const compressedPath = path.join(__dirname, "../", "uploads", "compressed", fileName);
                    //remove old image(big size image)
                    fs.unlink(toCompressPath, (err) => { if (err) reject(err) });
                    //move compressed image from compressed folder to images folder
                    fs.rename(compressedPath, destinationPath, (err) => { if (err) reject(err); });
                    // return 
                    resolve();
                }

            }
        );
    })


}
