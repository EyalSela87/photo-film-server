const User = require("../models/user");
const Album = require("../models/album");
const Response = require("../util/response");
// const bodyParser = require('body-parser');
const path = require("path");
const fs = require("fs");
const compress_images = require("compress-images");

const domain = "http://localhost:3000";
// const domain = "https://404040.co.il/firumu";

exports.createAlbum = (req, res, next) =>
{
  // get body info
  const info = {
    userId: req.body.id || null,
    name: req.body.name || null,
  };
  //validate info
  const errors = validateInfo(info);
  // throw error if there's any
  if (errors.length > 0)
  {
    const error = new Error(errors);
    error.statusCode = 422;
    throw error;
  }
  //transfer the id to a number
  info.userId = +info.userId;

  // get the user by his id
  User.findByPk(info.userId)
    .then((user) =>
    {
      // if no user found throw error to the client
      if (!user)
        throw new Error("couldn't find user by the id of " + info.userId);
      // create the album for the user
      return user.createAlbum(info);
    })
    .then((result) =>
    {
      // TODO add format for the creation date

      // inform the client for success album creation
      res.status(200).json(Response(true, result, null));
    })
    .catch((err) =>
    {
      err.statusCode = 422;
      next(err);
    });
};

exports.getAlbumsByUserId = (req, res, next) =>
{
  //get userId from params
  const userId = req.params.userId;
  //find the user by id
  User.findByPk(userId)
    .then((user) =>
    {
      //if no user fond throw error to the client
      if (!user) throw new Error("cannot find user by id of " + userId);
      // get all albums from the user
      return user.getAlbums();
    })
    .then((albums) =>
    {
      //send the albums to the client
      res.status(200).json(Response(true, { albums: albums }, null));
    })
    .catch((err) =>
    {
      err.statusCode = 422;
      next(err);
    });
};

exports.getAlbumById = (req, res, next) =>
{
  const albumId = +req.params.albumId;

  Album.findByPk(albumId)
    .then((album) =>
    {
      if (!album) throw new Error("cannot find album");
      res.status(200).json(Response(true, album, null));
    })
    .catch((err) =>
    {
      err.statusCode = 422;
      next(err);
    });
};

exports.uploadImage = (req, res, next) =>
{
  const debugPath = path.join(__dirname, '../', 'files', 'uploadImage' + '.txt');
  fs.writeFile(debugPath, "start", (err) => { if (err) throw err });
  console.log("uploadImage")
  // file validation
  if (!req.file)
  {
    const error = new Error("file does not sent");
    error.statusCode = 422;
    throw error;
  }

  // check validate file size
  let filePath = path.join(__dirname, "../", "images", req.file.filename);
  fs.stat(filePath, (err, fileStats) =>
  {
    if (err)
    {
      err.statusCode = 422;
      next(err);
    }
    // set file data
    const data = {
      path: domain + "/images/" + req.file.filename,
      name: req.file.filename,
    };
    console.log("uploadImage2", data.name)
    console.log("uploadImage2", req.file.filename)
    // check if comretion needed
    const fileSize = fileStats.size;
    const compressNeeded = fileSize > 1000000;
    // conpress file if needed send response
    if (compressNeeded)
    {
      console.log("compressNeeded", data.name)
      compressFile2(data.name)
        .then(() => 
        {
          console.log("path", req.file.fileName);
          res.status(200).json(Response(true, data, null));

        })
        .catch((err) => { err.statusCode = 422; next(err); });

    }
    else res.status(200).json(Response(true, data, null));

  });
};

exports.createImage = (req, res, next) =>
{
  const albumId = req.body.albumId;

  const imageData = {
    name: req.body.imageName || "",
    url: req.body.imageUrl || "",
    caption: req.body.caption || "",
    camera: req.body.camera || "",
    lense: req.body.lense || "",
    isoRate: req.body.isoRate || "",
    f_stop: req.body.f_stop || "",
    sutterSpeed: req.body.sutterSpeed || "",
    overExpose: req.body.overExpose || "",
    avg_light_shadow: req.body.avg_light_shadow || "",
    film: req.body.film || "",
  };

  Album.findByPk(albumId)
    .then((album) =>
    {
      if (!album) throw new Error("couldn't find album");
      return album.createImage(imageData);
    })
    .then((image) =>
    {
      console.log("image", image);
      res.status(200).json(Response(true, image, null));
    })
    .catch((err) =>
    {
      err.statusCode = 422;
      next(err);
    });
};

exports.getImageByAlbumId = (req, res, next) =>
{
  const albumId = req.params.albumId;

  Album.findByPk(albumId)
    .then((album) =>
    {
      if (!album) throw new Error("clouln't find the album");
      return album.getImages();
    })
    .then((images) =>
    {
      res.status(200).json(Response(true, { images: images }, null));
    })
    .catch((err) =>
    {
      err.statusCode = 422;
      next(err);
    });
};

const compressFile2 = (fileName) =>
{
  console.log("compressFile2")
  return new Promise((resolve, reject) =>
  {
    const filePath = path.join(__dirname, "../", "images", fileName);
    const newPath = path.join(__dirname, "../", "images", "toCpmpress", fileName);

    fs.rename(filePath, newPath, (err) => { if (err) reject(err); });


    const INPUT_path_to_your_images = "images/toCpmpress/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}";
    const OUTPUT_path = "images/compressed/";

    compress_images("images/toCpmpress/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}",
      "images/compressed/",
      { compress_force: false, statistic: true, autoupdate: true },
      false,
      { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
      { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
      { svg: { engine: "svgo", command: "--multipass" } },
      { gif: { engine: "gifsicle", command: ["--colors", "64", "--use-col=web"] } },
      function (error, completed, statistic)
      {
        console.log("-------------");
        console.log(error);
        console.log(completed);
        console.log(statistic);
        console.log("-------------");

        if (completed === true)
        {
          const compressedPath = path.join(__dirname, "../", "images", "compressed", fileName);
          fs.unlink(filePath, (err) => { if (err) reject(err) });
          fs.unlink(newPath, (err) => { if (err) reject(err) });
          fs.rename(compressedPath, filePath, (err) => { if (err) reject(err); });
          resolve();
        }

      }
    );
  })


}


const validateInfo = (info) =>
{
  const errors = [];
  if (!info.userId) errors.push("user id field is require");
  else if (isNaN(+info.userId)) errors.push("user id has to be a number");
  if (!info.name || info.name === "")
    errors.push("album name is field is require");
  else
  {
    const noSpacename = info.name.split(" ").join("");
    if (noSpacename.length < 3)
      errors.push("album name must contain at least 3 charecters");
  }

  return errors;
};
