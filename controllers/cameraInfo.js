const CameraInfo = require('../models/cameraInfo');
const filmInfo = require('../models/filmsInfo');
const Response = require("../util/response");

exports.postAddCamera = (req, res, next) =>
{
    const cameraName = req.body.name || null;

    CameraInfo.create({ name: cameraName })
        .then(result => res.status(200).json(Response(true, { cameraName: result }, null)))
        .catch((err) => { err.statusCode = 422; next(err); });
}

exports.getCameras = (req, res, next) =>
{
    CameraInfo.findAll()
        .then(result => res.status(200).json(Response(true, { cameraName: result }, null)))
        .catch((err) => { err.statusCode = 422; next(err); });
}

exports.postAddFilm = (req, res, next) =>
{
    const filmName = req.body.name || null;

    filmInfo.create({ name: filmName })
        .then(result => res.status(200).json(Response(true, { filmName: result }, null)))
        .catch((err) => { err.statusCode = 422; next(err); });
}

exports.getFilms = (req, res, next) =>
{
    filmInfo.findAll()
        .then(result => res.status(200).json(Response(true, { filmName: result }, null)))
        .catch((err) => { err.statusCode = 422; next(err); });
}

exports.postAddManyFilms = (req, res, next) =>
{
    console.log("postAddManyFilms")
    const films = req.body.films || null

    if (films === null)
    {
        const error = new Error("no films has sent");
        error.statusCode = 422;
        throw error;
    }

    const data = [];
    films.map(filmName => data.push({ name: filmName }));

    filmInfo.bulkCreate(data)
        .then(result =>
        {
            res.status(200).json(Response(true, { films: result }, null))
        })
        .catch((err) => { err.statusCode = 422; next(err); });

}

exports.postAddmanyCameras = (req, res, next) =>
{
    const cams = req.body.cameras || null;

    if (cams === null)
    {
        const error = new Error("no cameras has sent");
        error.statusCode = 422;
        throw error;
    }

    const data = [];
    console.log()
    cams.map(camName => data.push({ name: camName }));

    CameraInfo.bulkCreate(data)
        .then(result => 
        {
            res.status(200).json(Response(true, { cameras: result }, null));
        })
        .catch(err => { err.statusCode = 422; next(err) });
}