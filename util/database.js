const Sequelize = require('sequelize');

const sequelize = new Sequelize(
    'film_photo',
    'root',
    '',
    {
        host: 'localhost',
        dialect: 'mysql'
    });

module.exports = sequelize;