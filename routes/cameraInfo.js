const express = require('express');
const cameraInfoContoller = require('../controllers/cameraInfo');

const router = express.Router();

router.post('/addCamera', cameraInfoContoller.postAddCamera);

router.get('/getCameras', cameraInfoContoller.getCameras);

router.post('/addFilm', cameraInfoContoller.postAddFilm);

router.get('/getFilms', cameraInfoContoller.getFilms);

router.post('/addmanyFilms', cameraInfoContoller.postAddManyFilms);

router.post('/addmayCameras', cameraInfoContoller.postAddmanyCameras);

module.exports = router;