const express = require('express');
const albumContoller = require('../controllers/albums');

const router = express.Router();

router.post('/create', albumContoller.createAlbum);

router.get('/getAlbums/:userId', albumContoller.getAlbumsByUserId);

router.get('/getAlbum/:albumId', albumContoller.getAlbumById);

router.put('/uploadImage', albumContoller.uploadImage);

router.post('/createImage', albumContoller.createImage);

router.get('/getImages/:albumId', albumContoller.getImageByAlbumId);

module.exports = router;