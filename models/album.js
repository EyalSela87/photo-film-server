const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Album = sequelize.define('album',
    {
        id:
        {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        name: Sequelize.STRING,
    });

module.exports = Album;