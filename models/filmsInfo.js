const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const FilmInfo = sequelize.define('filmInfo',
    {
        id:
        {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        name: Sequelize.STRING,
    });

module.exports = FilmInfo;