const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Image = sequelize.define('image',
    {
        id:
        {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        name: Sequelize.STRING,
        url: Sequelize.STRING,
        caption: Sequelize.STRING,
        camera: Sequelize.STRING,
        lense: Sequelize.STRING,
        isoRate: Sequelize.STRING,
        f_stop: Sequelize.STRING,
        sutterSpeed: Sequelize.STRING,
        overExpose: Sequelize.STRING,
        avg_light_shadow: Sequelize.STRING,
        film: Sequelize.STRING,
    });

module.exports = Image;