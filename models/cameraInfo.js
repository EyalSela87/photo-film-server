const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const CameraInfo = sequelize.define('cameraInfo',
    {
        id:
        {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        name: Sequelize.STRING,
    });

module.exports = CameraInfo;