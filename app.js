//#region global imports
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const multer = require('multer');
const helmet = require('helmet');
const path = require('path');
const fs = require('fs');
//#endregion

//#region local imports
const sequelize = require('./util/database');
const userRoute = require('./routes/users');
const albumRoute = require('./routes/albums');
const cameraInfoRoute = require('./routes/cameraInfo');
const User = require('./models/user');
const Album = require('./models/album');
const FilesRoute = require('./routes/filesRoute');
const Image = require('./models/image');
const CameraInfo = require('./models/cameraInfo');
const filmInfo = require('./models/filmsInfo');
const Response = require('./util/response');
//#endregion
const PORT = 3000;

//#region middleware
const app = express();

const corsOptions =
{
    origin: '*',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Origin': '*',
    // 'Access-Control-Allow-Origin': 'http://localhost:8100',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
    // credentials: true,
    optionsSuccessStatus: 200
};
app.use(cors(corsOptions));
app.use(helmet());


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/images', express.static(path.join(__dirname, 'uploads', 'images')));
//#endregion

//#region Routes
app.use('/test', (req, res, next) =>
{
    res.status(200).json(Response(true, "test123", null));
})


app.use('/users', userRoute);
app.use('/albums', albumRoute);
app.use('/cameraInfo', cameraInfoRoute);
app.use('', FilesRoute);
//#endregion

//Error Handling
app.use((error, req, res, next) =>
{
    console.log("eorrrrr")
    res.status(error.statusCode).json(Response(false, null, error.message))
});

//#region Sequelize Relations
User.hasMany(Album);
Album.hasMany(Image);
//#endregion

// sequelize.sync({ force: true })
sequelize.sync()
    .then(result => app.listen(3000))
    .catch(err => console.log(err));